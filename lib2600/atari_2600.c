#include "atari_2600.h"
#include "mos_6507.h"

void Atari2600_initialize(Atari2600* self, Mos6507_JitCache* jit_cache,
                          const uint8_t* rom_data, const uint16_t rom_size) {
  uint16_t i;
  Mos6507_initialize(&self->cpu, jit_cache);
  Ram_initialize(&self->ram);
  Mos6507_add_memory_device(&self->cpu, &self->ram.memory_device, 0x80, 0x80);
  Rom_initialize(&self->rom, rom_data);
  for (i = 0x1000; i < 0x2000; i += rom_size) {
    Mos6507_add_memory_device(&self->cpu, &self->rom.memory_device, i,
                              rom_size);
  }
}

void Atari2600_run(Atari2600* self) {
  Mos6507_reset(&self->cpu);
}
