  .global Ram_peek
Ram_peek:
  ldr  r2, [r0, #12]  /* r2: base ARM address for the RAM  */
  ldrb r0, [r2, r1]
  bx   lr

  .global Ram_peek_word
Ram_peek_word:
  ldr  r2, [r0, #12]  /* r2: base ARM address for the RAM  */
  ldrh r0, [r2, r1]
  bx   lr
