#include <stdio.h>
#include "ram.h"

void Ram_initialize(Ram* self) {
  MemoryDevice_initialize(&self->memory_device);
  self->memory_device.peek = Ram_peek;
  self->memory_device.peek_word = Ram_peek_word;
  self->memory_device.poke = Ram_poke;
  self->memory_device.poke_word = Ram_poke_word;
}

void Ram_poke(MemoryDevice* memory_device, const uint16_t address,
              const uint8_t value) {
  Ram* self = (Ram*) memory_device;
  printf("Poke $%02hhX into $%04hX\n", value, address);
  self->data[address] = value;
}

void Ram_poke_word(MemoryDevice* memory_device, const uint16_t address,
                   const uint16_t value) {
  Ram* self = (Ram*) memory_device;
  printf("Poke $%04hX into $%04hX\n", value, address);
  *(uint16_t*) &self->data[address] = value;
}
