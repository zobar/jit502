#include "rom.h"

void Rom_initialize(Rom* self, const uint8_t* data) {
  MemoryDevice_initialize(&self->memory_device);
  self->data = data;
  self->memory_device.peek = Rom_peek;
  self->memory_device.peek_word = Rom_peek_word;
}
