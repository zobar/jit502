  .global Rom_peek
Rom_peek:
  ldr  r2, [r0, #16]  /* r2: base ARM address for the ROM  */
  ldrb r0, [r2, r1]
  bx   lr

  .global Rom_peek_word
Rom_peek_word:
  ldr  r2, [r0, #16]  /* r2: base ARM address for the ROM  */
  ldrh r0, [r2, r1]
  bx   lr
