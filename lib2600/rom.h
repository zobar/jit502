#ifndef ROM_H
#define ROM_H

#include "mos_6507_memory_device.h"

typedef struct Rom {
  MemoryDevice   memory_device;
  const uint8_t* data;
} Rom;

void Rom_initialize(Rom* self, const uint8_t* data);
uint8_t Rom_peek(MemoryDevice* device, const uint16_t address);
uint16_t Rom_peek_word(MemoryDevice* device, const uint16_t address);

#endif
