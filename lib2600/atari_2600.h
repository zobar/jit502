#ifndef ATARI_2600_H
#define ATARI_2600_H

#include <stdint.h>
#include "mos_6507.h"
#include "ram.h"
#include "rom.h"

typedef struct Atari2600 {
  Mos6507 cpu;
  Ram     ram;
  Rom     rom;
} Atari2600;

void Atari2600_initialize(Atari2600* self, Mos6507_JitCache* jit_cache,
                          const uint8_t* rom_data, const uint16_t rom_size);
void Atari2600_run(Atari2600* self);

#endif
