#ifndef RAM_H
#define RAM_H

#include "mos_6507_memory_device.h"

typedef struct Ram {
  MemoryDevice memory_device;
  uint8_t      data[128];
} Ram;

void Ram_initialize(Ram* self);
uint8_t Ram_peek(MemoryDevice* device, const uint16_t address);
uint16_t Ram_peek_word(MemoryDevice* device, const uint16_t address);
void Ram_poke(MemoryDevice* device, const uint16_t address,
              const uint8_t value);
void Ram_poke_word(MemoryDevice* device, const uint16_t address,
                   const uint16_t value);

#endif
