#include "mos_6507_memory_device.h"

uint16_t MemoryDevice_default_peek_word(MemoryDevice* self, uint16_t address);
uint16_t MemoryDevice_default_poke_word(MemoryDevice* self, uint16_t address,
                                        uint16_t value);
uint8_t MemoryDevice_null_peek(MemoryDevice* self, uint16_t address);
void MemoryDevice_null_poke(MemoryDevice* self, uint16_t address,
                            uint8_t value);

void MemoryDevice_initialize(MemoryDevice* self) {
  self->peek = MemoryDevice_null_peek;
  self->peek_word = MemoryDevice_default_peek_word;
  self->poke = MemoryDevice_null_poke;
  self->poke_word = MemoryDevice_default_poke_word;
}
