  .global MemoryDevice_default_peek_word
MemoryDevice_default_peek_word:
                                   /* r0: MemoryDevice* self        */
                                   /* r1: uint16_t relative_address */
  ldr   r3, [r0]                   /* r3: peek                      */
  stmfd sp!, {r0, r1, r3, r4, lr}
  add   r1, #1
  blx   r3
  mov   r4, r0                     /* r4: hi byte (addr + 1)        */
  ldmfd sp!, {r0, r1, r3}
  blx   r3                         /* r0: lo byte                   */
  orr   r0, r4, LSL #8             /* r0: (hi << 8) | lo            */
  ldmfd sp!, {r4, pc}

  .global MemoryDevice_default_poke_word
MemoryDevice_default_poke_word:
                                   /* r0: MemoryDevice* self        */
                                   /* r1: uint16_t relative_address */
                                   /* r2: uint16_t value            */
  ldr   r3, [r0, #8]               /* r3: poke                      */
  stmfd sp!, {r0, r1, r2, r3, lr}
  uxtb  r2, r2                     /* r2: lo byte                   */
  blx   r3
  ldmfd sp!, {r0, r1, r2, r3}
  add   r1, #1                     /* r1: relative_address + 1      */
  uxtb  r2, r2, ROR #8             /* r2: hi byte                   */
  blx   r3
  ldmfd sp!, {pc}

  .global MemoryDevice_null_peek
MemoryDevice_null_peek:
  mov r0, #0
  bx  lr

  .global MemoryDevice_null_poke
MemoryDevice_null_poke:
  bx lr
