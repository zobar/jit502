#ifndef MOS_6507_RUNTIME_H
#define MOS_6507_RUNTIME_H

typedef Mos6507* (*RuntimeFunction)(Mos6507* self, uint8_t arg);

Mos6507* Mos6507_Runtime_JMP(Mos6507* self, uint16_t address);
Mos6507* Mos6507_Runtime_JSR(Mos6507* self, uint32_t addresses);
Mos6507* Mos6507_Runtime_STA_zpx(Mos6507* self, uint8_t index);

#endif
