#ifndef MOS_6507_JIT_H
#define MOS_6507_JIT_H

void Mos6507_jit(Mos6507* self, uint16_t address);

#endif
