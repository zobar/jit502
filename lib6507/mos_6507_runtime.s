  .global Mos6507_Runtime_JSR
Mos6507_Runtime_JSR:
                              /* r0: Mos6507* self       */
                              /* r1 lo: uint16_t address */
                              /* r1 hi: uint16_t return  */
                              /* r7: uint16_t s          */
  stmfd sp!, {r0, r1, lr}
  mov   r2, r1, LSR #16       /* r2: return address      */
  sub   r1, r7, #1            /* r1: stack address       */
  sub   r7, #2
  bl    Mos6507_poke_word
  ldmfd sp!, {r0, r1, lr}
                              /* fall through . . .      */

  .global Mos6507_Runtime_JMP
Mos6507_Runtime_JMP:
                              /* r0: Mos6507* self    */
                              /* r1: uint16_t address */
  mov   r1, r1, LSL #19
  mov   r1, r1, LSR #19       /* r1: 8k address       */
  add   r2, r0, #0x800        /* r2: jumps            */
  add   r2, r2, r1, LSL #2    /* r2: jumps[address]   */
  ldr   r3, [r2]              /* r3: *jumps[address]  */
  cmp   r3, #0
  bxne  r3
  stmfd sp!, {r0, r2, lr}
  bl    Mos6507_jit
  ldmfd sp!, {r0, r2, lr}
  ldr   r3, [r2]
  bx    r3

  .global Mos6507_Runtime_STA_zpx
Mos6507_Runtime_STA_zpx:
                       /* r0: Mos6507* self */
                       /* r1: uint8_t  base */
                       /* r4: uint8_t  a    */
                       /* r5: uint8_t  x    */
  add   r1, r1, r5
  uxtb  r1, r1         /* r1: address       */
  mov   r2, r4         /* r2: value         */
  stmfd sp!, {r0, lr}
  bl    Mos6507_poke
  ldmfd sp!, {r0, pc}
