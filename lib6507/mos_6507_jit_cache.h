#ifndef MOS_6507_JIT_CACHE_H
#define JIT_CACHE_H

#include <stdint.h>

#define Mos6507_MAX_EXPANSION 8

typedef struct Mos6507_JitCache {
  uint8_t data[8192 * Mos6507_MAX_EXPANSION];
} Mos6507_JitCache;

void Mos6507_JitCache_initialize(Mos6507_JitCache* self);

#endif
