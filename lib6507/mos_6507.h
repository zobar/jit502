#ifndef MOS_6507_H
#define MOS_6507_H

#include "mos_6507_memory_device.h"
#include "mos_6507_jit_cache.h"

typedef struct Mos6507 Mos6507;

typedef struct Mos6507_DeviceInfo {
  MemoryDevice* memory_device;
  uint32_t      offset;
} Mos6507_DeviceInfo;

struct Mos6507 {
  Mos6507_DeviceInfo device_info[256];
  void*              jumps[8192];
  Mos6507_JitCache*  jit_cache;
  MemoryDevice       null_device;
};

void     Mos6507_add_memory_device(Mos6507* self, MemoryDevice* device,
                                   uint16_t offset, uint16_t length);
void     Mos6507_call             (Mos6507* self, uint16_t address);
void     Mos6507_initialize       (Mos6507* self, Mos6507_JitCache* jit_cache);
uint8_t  Mos6507_peek             (Mos6507* self, uint16_t address);
uint16_t Mos6507_peek_word        (Mos6507* self, uint16_t address);
uint8_t  Mos6507_poke             (Mos6507* self, uint16_t address,
                                   uint8_t value);
void     Mos6507_reset            (Mos6507* self);

#endif
