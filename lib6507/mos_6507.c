#include <stddef.h>
#include "mos_6507.h"

typedef void (*CompiledFunction)(Mos6507* self);
void Mos6507_trampoline(Mos6507* self, CompiledFunction function);

void Mos6507_add_memory_device(Mos6507* self, MemoryDevice* device,
                               uint16_t offset, uint16_t length) {
  int i;
  int index_offset = offset >> 5;
  int index_end = index_offset + (length >> 5);
  for (i = index_offset; i < index_end; ++i) {
    Mos6507_DeviceInfo* device_info = &self->device_info[i];
    device_info->offset = offset;
    device_info->memory_device = device;
  }
}

void Mos6507_call(Mos6507* self, uint16_t address) {
  CompiledFunction* jumps = (CompiledFunction*) self->jumps;
  CompiledFunction jump = jumps[address &= 0x1fff];
  if (!jump) {
    Mos6507_jit(self, address);
    jump = jumps[address];
  }
  Mos6507_trampoline(self, jump);
}

void Mos6507_initialize(Mos6507* self, Mos6507_JitCache* jit_cache) {
  int i = 0;
  self->jit_cache = jit_cache;
  for (i = 0; i < 8192; ++i)
    self->jumps[i] = NULL;
  MemoryDevice_initialize(&self->null_device);
  Mos6507_add_memory_device(self, &self->null_device, 0, 0x2000);
}

void Mos6507_reset(Mos6507* self) {
  Mos6507_call(self, Mos6507_peek_word(self, 0xfffc));
}
