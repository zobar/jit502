#include <stdbool.h>
#include <stdio.h>
#include "mos_6507.h"
#include "mos_6507_runtime.h"

typedef _Bool (*JitFunction)(Mos6507* self, uint16_t* src, uint8_t** dest);

#define jit_imm(name)                                                         \
static _Bool jit_##name##_imm(Mos6507* self, uint16_t* src, uint8_t** dest) { \
  uint8_t imm = Mos6507_peek(self, ++(*src));                                 \
  printf("%p $%04hX " #name " $%02hhX\n", *dest, *src - 1, imm);

#define jit_imp(name)                                                   \
static _Bool jit_##name(Mos6507* self, uint16_t* src, uint8_t** dest) { \
  printf("%p $%04hX " #name "\n", *dest, *src);

#define jit_rel(name) \
static _Bool jit_##name(Mos6507* self, uint16_t* src, uint8_t** dest) { \
  int8_t rel = Mos6507_peek(self, ++(*src));                            \
  int16_t abs = *src + rel + 1;                                         \
  printf("%p $%04hX " #name " $%04hX\n", *dest, *src, abs);

#define jit_zpx(name)                                                         \
static _Bool jit_##name##_zpx(Mos6507* self, uint16_t* src, uint8_t** dest) { \
  uint8_t base = Mos6507_peek(self, ++(*src));                                \
  printf("%p $%04hX " #name " $%02hhX,X\n", *dest, *src - 1, base);

#define jit_emit_byte(byte) *(*dest)++ = (uint8_t) byte
#define jit_emit(halfword)  *(*(uint16_t**)dest)++ = (uint16_t) halfword
#define jit_emit_word(word) *(*(uint32_t**)dest)++ = (uint32_t) word

#define jit_emit_break()    jit_emit(0xbe00)

#define jit_realign         (int) *dest % 4
#define jit_continue(cycles) \
  return true;               \
}

static void jit_emit_function(uint16_t* src, uint8_t** dest,
                              RuntimeFunction function) {
  jit_emit(0x4a01);        /* ldr r2, &function */
  jit_emit(0x4790);        /* blx r2            */
  if (jit_realign)
    jit_emit(0xe001);      /* b   end           */
  else {
    jit_emit(0xe002);      /* b   end           */
    jit_emit(0x0000);
  }
  jit_emit_word(function); /* .word function    */
}

jit_rel(BNE) {
  if (jit_realign) {
    jit_emit(0xd006);  /* beq end */
    jit_emit(0x4902);  /* ldr r1, branch_to */
    jit_emit(0x4a01);  /* ldr r2, function  */
    jit_emit(0x4710);  /* bx  r2            */
    jit_emit(0x0000);
  }
  else {
    jit_emit(0xd005);  /* beq end */
    jit_emit(0x4902);  /* ldr r1, branch_to */
    jit_emit(0x4a00);  /* ldr r2, function  */
    jit_emit(0x4710);  /* bx  r2            */
  }
  jit_emit_word(Mos6507_Runtime_JMP);
  jit_emit(abs);
  jit_continue(2); /* 3 if succeeds, 4 if to a new page */
}

jit_imp(CLD) {
  /* no-op - BCD arithmetic not yet supported */
  jit_continue(2);
}

jit_imp(DEX) {
  jit_emit(0x3d01);  /* sub r5, #1 */
  jit_continue(2);
}

jit_imp(INX) {
  jit_emit(0x3501);  /* add r5, #1 */
  jit_continue(2);
}

static _Bool jit_JSR(Mos6507* self, uint16_t* src, uint8_t** dest) {
  int16_t abs = Mos6507_peek_word(self, ++(*src));
  (*src) += 2;
  printf("%p $%04hX " "JSR" " $%04hX\n", *dest, *src, abs);
  if (jit_realign) {
    jit_emit(0x4902);  /* ldr r1, branch_to */
    jit_emit(0x4a00);  /* ldr r2, function  */
    jit_emit(0x4710);  /* bx  r2            */
  }
  else {
    jit_emit(0x4902);  /* ldr r1, branch_to */
    jit_emit(0x4a01);  /* ldr r2, function  */
    jit_emit(0x4710);  /* bx  r2            */
    jit_emit(0x0000);
  }
  jit_emit_word(Mos6507_Runtime_JSR);
  jit_emit(abs);
  jit_emit(*src);
  return false;
  // jit_continue(6);
}

jit_imm(LDA) {
  jit_emit_byte(imm);  /* mov r4, imm */
  jit_emit_byte(0x24);
  jit_continue(2);
}

jit_imm(LDX) {
  jit_emit_byte(imm);  /* mov r5, imm */
  jit_emit_byte(0x25);
  jit_continue(2);
}

jit_imp(SEI) {
  jit_continue(2);
}

jit_zpx(STA) {
  jit_emit_byte(base);  /* mov r1, imm */
  jit_emit_byte(0x21);
  jit_emit_function(src, dest, Mos6507_Runtime_STA_zpx);
  jit_continue(4);
}

jit_imp(TXS) {
  jit_emit(0x1c2f);  /* mov r7, r5 */
  jit_continue(2);
}

static _Bool jit_illegal(Mos6507* self, uint16_t* src, uint8_t** dest) {
  uint8_t op = Mos6507_peek(self, *src);
  printf("%p $%04hX $%02hhX\n", *dest, *src, op);
  jit_emit(0xbdf0);  /* pop {r4-r7, pc} */
  return false;
}

static JitFunction jit_functions[256] = {
  /* 00 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 10 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 20 */
  jit_JSR,     NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 30 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 40 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 50 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 60 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 70 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  jit_SEI,     NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 80 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* 90 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        jit_STA_zpx, NULL,        NULL,
  NULL,        NULL,        jit_TXS,     NULL,
  NULL,        NULL,        NULL,        NULL,

  /* A0 */
  NULL,        NULL,        jit_LDX_imm, NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        jit_LDA_imm, NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* B0 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* C0 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        jit_DEX,     NULL,
  NULL,        NULL,        NULL,        NULL,

  /* D0 */
  jit_BNE,     NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  jit_CLD,     NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* E0 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  jit_INX,     NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,

  /* F0 */
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL,
  NULL,        NULL,        NULL,        NULL
};

void Mos6507_jit(Mos6507* self, uint16_t address) {
  _Bool cont = true;
  uint8_t* dest = &self->jit_cache->data[address * Mos6507_MAX_EXPANSION];
  uint16_t start = address;
  while (cont) {
    uint8_t op = Mos6507_peek(self, address);
    JitFunction jitter = jit_functions[op];
    if (!jitter)
      jitter = jit_illegal;
    self->jumps[address] = dest + 1; /* +1 shifts to Thumb */
    cont = jitter(self, &address, &dest);
    ++address;
  }
}
