#ifndef MEMORY_DEVICE_H
#define MEMORY_DEVICE_H

#include <stdint.h>

typedef struct MemoryDevice MemoryDevice;

typedef uint8_t  (*MemoryDevicePeek    )(MemoryDevice* self,
                                         uint16_t      relative_address);
typedef uint16_t (*MemoryDevicePeekWord)(MemoryDevice* self,
                                         uint16_t      relative_address);
typedef void     (*MemoryDevicePoke    )(MemoryDevice* self,
                                         uint16_t      relative_address,
                                         uint8_t       value);
typedef void     (*MemoryDevicePokeWord)(MemoryDevice* self,
                                         uint16_t      relative_address,
                                         uint16_t      value);

struct MemoryDevice {
  MemoryDevicePeek     peek;
  MemoryDevicePeekWord peek_word;
  MemoryDevicePoke     poke;
  MemoryDevicePokeWord poke_word;
};

void MemoryDevice_initialize(MemoryDevice* self);

#endif
