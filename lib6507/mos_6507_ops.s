  .global Mos6507_peek
Mos6507_peek:
                           /* r0: Mos6507* self          */
                           /* r1: uint16_t address       */
  mov  r1, r1, LSL #19
  mov  r1, r1, LSR #19     /* r1: 8k address             */
  mov  r3, r1, LSR #5
  add  r3, r0, r3, LSL #3  /* r3: Mos6507_DeviceInfo*    */
  ldr  r0, [r3]            /* r0: MemoryDevice*          */
  ldr  r3, [r3, #4]        /* r3: base device address    */
  rsb  r1, r3, r1          /* r1: relative poke address  */
  ldr  pc, [r0]            /* peek(device, address)      */

  .global Mos6507_peek_word
Mos6507_peek_word:
                           /* r0: Mos6507* self          */
                           /* r1: uint16_t address       */
  mov  r1, r1, LSL #19
  mov  r1, r1, LSR #19     /* r1: 8k address             */
  mov  r3, r1, LSR #5
  add  r3, r0, r3, LSL #3  /* r3: Mos6507_DeviceInfo*    */
  ldr  r0, [r3]            /* r0: MemoryDevice*          */
  ldr  r3, [r3, #4]        /* r3: base device address    */
  rsb  r1, r3, r1          /* r1: relative poke address  */
  ldr  pc, [r0, #4]        /* peek_word(device, address) */

  .global Mos6507_poke
Mos6507_poke:
                           /* r0: Mos6507* self          */
                           /* r1: uint16_t address       */
                           /* r2: uint8_t  value         */
  mov  r1, r1, LSL #19
  mov  r1, r1, LSR #19     /* r1: 8k address             */
  mov  r3, r1, LSR #5
  add  r3, r0, r3, LSL #3  /* r3: Mos6507_DeviceInfo*    */
  ldr  r0, [r3]            /* r0: MemoryDevice*          */
  ldr  r3, [r3, #4]        /* r3: base device address    */
  rsb  r1, r3, r1          /* r1: relative poke address  */
  ldr  pc, [r0, #8]        /* poke(device, address)      */

  .global Mos6507_poke_word
Mos6507_poke_word:
                           /* r0: Mos6507* self          */
                           /* r1: uint16_t address       */
                           /* r2: uint16_t value         */
  mov  r1, r1, LSL #19
  mov  r1, r1, LSR #19     /* r1: 8k address             */
  mov  r3, r1, LSR #5
  add  r3, r0, r3, LSL #3  /* r3: Mos6507_DeviceInfo*    */
  ldr  r0, [r3]            /* r0: MemoryDevice*          */
  ldr  r3, [r3, #4]        /* r3: base device address    */
  rsb  r1, r3, r1          /* r1: relative poke address  */
  ldr  pc, [r0, #12]       /* poke_word(device, address) */

  .global Mos6507_trampoline
Mos6507_trampoline:
  push {r4-r7, lr}
  bx   r1
