#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "atari_2600.h"

int main(int argc, char* argv[]) {
  int result = EXIT_FAILURE;
  if (argc != 2)
    printf("Usage: %s romfile\n", argv[0]);
  else {
    int rom_fd = open(argv[1], O_RDONLY);
    if (rom_fd != -1) {
      struct stat rom_stat;
      if (fstat(rom_fd, &rom_stat) != -1) {
        off_t rom_size = rom_stat.st_size;
        uint8_t* rom = mmap(NULL, rom_size, PROT_READ, MAP_PRIVATE, rom_fd, 0);
        if (rom != MAP_FAILED) {
          Mos6507_JitCache* jit_cache = mmap(NULL, sizeof(Mos6507_JitCache),
                                             PROT_EXEC | PROT_READ | PROT_WRITE,
                                             MAP_ANONYMOUS | MAP_PRIVATE, -1,
                                             0);
          if (jit_cache != MAP_FAILED) {
            Atari2600* atari_2600 = malloc(sizeof(Atari2600));
            if (atari_2600) {
              Mos6507_JitCache_initialize(jit_cache);
              Atari2600_initialize(atari_2600, jit_cache, rom, rom_size);
              Atari2600_run(atari_2600);
              puts("Done!");
              result = EXIT_SUCCESS;
              free(atari_2600);
            }
            munmap(jit_cache, sizeof(Mos6507_JitCache));
          }
          munmap(rom, rom_size);
        }
      }
      close(rom_fd);
    }
    if (result != EXIT_SUCCESS)
      perror(argv[1]);
  }

  return result;
}
